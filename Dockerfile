FROM fedora

RUN dnf -y install texlive \
    texlive-latex \
    texlive-xetex \
    texlive-collection-latex \
    texlive-collection-latexrecommended \
    texlive-xetex-def \
    texlive-collection-xetex \
    latexmk \
    neovim \
    texlive-noto \
    texlive-mfirstuc \
    texlive-lipsum \
    evince \
    git \
    python3-pip

RUN pip3 install neovim-remote

WORKDIR /app
